using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerController : MonoBehaviour
{
    public Animator animator;
    private int counter = 0;
    private float timer = 0f;
    public GameObject computer;
    private bool leftDown = false;
    private bool rightDown = false;
    private Vector3 startPosition;
    public AudioSource[] smash;
    private AudioSource music;

    private void Start()
    {
        music = GameObject.FindGameObjectWithTag("GameController").GetComponent<Manager>().music;
        startPosition = computer.transform.position;
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        {
            if (rightDown)
            {
                CounterUp();
                timer = 0;
            }
            else
            {
                leftDown = true;
                timer = 0;
            }
        }
        else if (Input.GetKeyDown(KeyCode.L))
        {
            if(leftDown)
            {
                CounterUp();
                timer = 0;
            }
            else
            {
                rightDown = true;
                timer = 0;
            }
        }

        else
        {
            timer += Time.deltaTime;
            if(timer >1)
            {
                animator.SetBool("Headbanging", false);
                leftDown = false;
                rightDown = false;
                music.Pause();
            }
        }

    }

    private void CounterUp()
    {
       
        if (counter == 0)
        {
            music.Play();
        }

        counter++;
        animator.SetBool("Headbanging", true);
        computer.GetComponent<PcState>().Smash(counter);
        if(!music.isPlaying)
        {
            music.UnPause();
        }
    }

    public void HeadOnTable(AnimationEvent e)
    {
        smash[(UnityEngine.Random.Range(0, smash.Length))].Play();
        Tween Shaker = computer.transform.DOShakePosition(.7f, 0.2f, 10, 90f, false, false);
        Shaker.SetAutoKill(false);
       Shaker.OnComplete(() => computer.transform.position=startPosition);

       
    }

}
