using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Manager : MonoBehaviour
{
    public GameObject[] devices;
    private int smashed = 0;
    public AudioSource music;
    public AudioSource grump;
    public GameObject bubble;
    private bool gameOver = false;

    void Update()
    {
        smashed = 0;

        for (int i = 0; i < devices.Length; i++)
        {
            if (devices[i].GetComponent<ISmashable>().isSmashed())
            {
                Debug.Log(devices[i] +" smashed");
                smashed++;
            }
        }

        if(smashed==devices.Length && !gameOver)
        {
            gameOver = true;
            bubble.SetActive(true);
            grump.Play();
            StartCoroutine(wait());  
        }

    }

    IEnumerator wait()
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("Ending");
    }

}
