using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PcState : MonoBehaviour, ISmashable
{
    public GameObject[] pcStates;

    public bool isSmashed()
    {
        return pcStates[2].activeSelf;
    }

    public void Smash(int count)
    {
        if(count == 30)
        {
            pcStates[0].SetActive(false);
            pcStates[1].SetActive(true);
            pcStates[2].SetActive(false);
        }

        else if (count >= 50)
        {
            pcStates[0].SetActive(false);
            pcStates[1].SetActive(false);
            pcStates[2].SetActive(true);
        }
    }

    void Update()
    {
        
    }
}
