using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class Bin : MonoBehaviour, ISmashable
{

    public GameObject[] deviceState;
    private Vector3 startPosition;
    public AudioSource bin;


    private int count = 0;

    private void Start()
    {
       
        startPosition = this.transform.position;
    }


    public void Smash(int count)
    {
        if (count == 2)
        {
            bin.Play();
            deviceState[0].SetActive(false);
            deviceState[1].SetActive(true);

        }

    }

    void OnMouseOver()
    {
        if (Input.GetMouseButtonDown(0))
        {
            count++;
            Smash(count);
            Tween Shaker = this.transform.DOShakePosition(.7f, 0.2f, 10, 90f, false, false);
            Shaker.SetAutoKill(false);
            Shaker.OnComplete(() => this.transform.position = startPosition);

        }

    }

    public bool isSmashed()
    {
        
        return count >= 2;
    }
}