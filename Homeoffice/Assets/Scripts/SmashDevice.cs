using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;

public class SmashDevice : MonoBehaviour, ISmashable
{
    public GameObject[] deviceState;
    private Vector3 startPosition;
    public AudioSource[] boom;
    private int count = 0;

    private void Start()
    {
        startPosition = this.transform.position;
    }

    public void Smash(int count)
    {
        if (count == 10)
        {
            deviceState[0].SetActive(false);
            deviceState[1].SetActive(true);
            deviceState[2].SetActive(false);
        }

        else if (count >= 20)
        {
            deviceState[0].SetActive(false);
            deviceState[1].SetActive(false);
            deviceState[2].SetActive(true);

        }
    }

    void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(0))
        {
            boom[(Random.Range(0,boom.Length))].Play();
            count++;
            Smash(count);
            Tween Shaker = this.transform.DOShakePosition(.7f, 0.2f, 10, 90f, false, false);
            Shaker.SetAutoKill(false);
            Shaker.OnComplete(() => this.transform.position = startPosition);

        }
    }

    public bool isSmashed()
    {
        return count >= 2;
    }
}